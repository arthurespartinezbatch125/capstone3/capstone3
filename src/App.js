import React, {useState, useEffect} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';


import UserContext from './UserContext'

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'

export default function App(){

	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

	const unsetUser =() => {
		localStorage.clear()
		setUser({
			id: null,
			isAdmin: null
		})
	}

	useEffect(()=>{
		let token = localStorage.getItem('token')
		fetch('https://salty-scrubland-98294.herokuapp.com/api/users/details', {
			method: 'GET',
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			if(typeof result._id !== "undefined"){
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})
	}, [])

	return(
		<UserContext.Provider value={{user, setUser, unsetUser}}>
			<BrowserRouter >
				<AppNavbar/>
				<Switch >
					<Route exact path="/" component={Home}/>
				</Switch>
			</BrowserRouter>
		</UserContext.Provider>
	)
}