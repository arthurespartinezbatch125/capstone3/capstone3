import React from 'react';
// CSS
import './../components/banner.css'
import './../components/product.css';
//components
import Banner from './../components/Banner';
import Products from './../components/Products' 

export default function Home(){
	return(
		<div style={{margin: 0, padding: 0}}>
			<Banner />
			<Products />
		</div>
	)
}
/*<Products style={{width: '100%'}}/>
// <Banner style={{width: '100%'}}/>*/