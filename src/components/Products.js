import React, {useState, useEffect, useContext} from 'react';
// UserContext
import UserContext from './../UserContext';

// components
import UserView from './../components/UserView'
import AdminView from './../components/AdminView'


export default function Products(){

	
	const[products, setProducts] = useState([])
	const {user} = useContext(UserContext);

	const fetchData = () => {

		fetch('https://salty-scrubland-98294.herokuapp.com/api/products/all', {
			method: "GET",
		})
		.then(result => result.json())
		.then(result => {
			setProducts(result)
		})
	}
	useEffect(()=>{
		fetchData()
	}, [])
	
  
	return(
		<div>
			{(user.isAdmin !== true || user === null) ? <UserView productData = {products}/>  :
			<AdminView productData = {products} fetchData={fetchData}/>}
		</div>
	)
}