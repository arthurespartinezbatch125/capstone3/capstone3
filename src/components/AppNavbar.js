import React, {Fragment, useContext, useState, useEffect} from 'react';
import {useHistory, Link, Redirect} from 'react-router-dom';
import UserContext from './../UserContext';
import './nav.css';

// alert
import Swal from 'sweetalert2';
//bootstrap
import {Navbar, Nav, Form, Button, FormControl, NavDropdown, Modal} from 'react-bootstrap';
// icons
import {FaHome, FaUsers, FaShoppingCart} from 'react-icons/fa';
import { AiOutlineTransaction } from "react-icons/ai";
import { VscAccount } from "react-icons/vsc";
import { BsSearch } from "react-icons/bs";

export default function AppNavbar(){

	const [email,setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [firstName,setFirstName] = useState('');
	const [lastName,setLastName] = useState('');
	const [verifyPassword, setverifyPassword] = useState('');

	const {user, setUser, unsetUser} = useContext(UserContext);
	const [showLogin, setShowLogin] = useState(false);
	const [showSignup, setShowSignup] = useState(false);

	const openLogin = () => setShowLogin(true);
	const closeLogin = () => setShowLogin(false);
	const openSignup = () => {
		setShowSignup(true);
		setShowLogin(false)
	}
	const closeSignup = () => setShowSignup(false);

	// LOGIN
	function login(e){
		e.preventDefault();

		fetch('https://salty-scrubland-98294.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(result => {

			if(typeof result.access !== "undefined"){
				//What should we do with the aceess token
				localStorage.setItem('token', result.access)
				userDetails(result.access);
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "You have succesfully log in"
				})

			} else {
				fetch('https://salty-scrubland-98294.herokuapp.com/api/users/checkEmail', {
					method: "POST",
					headers:{
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						email: email
					})
				})
				.then(result => result.json())
				.then(result => {
					if(result === false){
						Swal.fire({
							title: "Email does not exist",
							icon: "error",
							text: "You do not have an existing account"
						})
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})
					}
				})
			}

		})

		const userDetails = (token) => {
			fetch('https://salty-scrubland-98294.herokuapp.com/api/users/details', {
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result=>result.json())
			.then(result => {
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			})
		}

		setEmail('');
		setPassword('');
		setShowLogin(false)
	}

	// Sign up
	function register(e){
		e.preventDefault();

		fetch('https://salty-scrubland-98294.herokuapp.com/api/users/checkEmail',{
				method: "POST",
				headers:{
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(result => result.json())
			.then(result =>{
				if(result ===  true){
					Swal.fire({
						title: 'Duplicate email found' ,
						icon: 'error' ,
						text: 'Please choose another email'
					})
				} else if (password !== verifyPassword) {
					Swal.fire({
						title: 'Incorrect password' ,
						icon: 'error' ,
						text: 'Password don\'t match'
					})
				} else if (password.length < 8) {
					Swal.fire({
						title: 'Insufficient Password' ,
						icon: 'error' ,
						text: 'Your password must be at least 8 characters'
					})
				} else {
					fetch('https://salty-scrubland-98294.herokuapp.com/api/users/register',{
						method: "POST",
						headers: {
							"Content-Type": 'application/json',
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password
						})
					})
					.then(result => result.json())
					.then(result => {
						if(result === true){
							Swal.fire({
								title: 'Registration Succesful',
								icon: 'success',
								text: 'You may now proceed to log in'
							})

							history.push('/');
						} else {
							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})
						}
					})
				}
			})

		setFirstName('');
		setLastName('');
		setEmail('');
		setPassword('');
		setverifyPassword('');
		setShowSignup(false)
	}

	//LOG OUT
	let history = useHistory();

	const logout =() => {
		unsetUser();
		history.push('/')
	}

	let dropNav = (user.id!==null) ? 
		<Fragment>
			<NavDropdown.Item href="#action/3.1">Profile</NavDropdown.Item>
			<NavDropdown.Divider />
			<NavDropdown.Item onClick={logout}>Log out</NavDropdown.Item>
		</Fragment>

		: 

		<Fragment>
			<NavDropdown.Item onClick={openLogin}>Log in</NavDropdown.Item>
			<NavDropdown.Divider />
			<NavDropdown.Item onClick={openSignup}>Sign up</NavDropdown.Item>
		</Fragment>

	let cart = (user.id!==null) ? (user.isAdmin === true) ? 
		<Fragment>
			<Nav.Link href="#features" className="mx-4"><AiOutlineTransaction size={25} className="nav-item"/></Nav.Link>
			<Nav.Link href="#features" className="mx-4"><FaUsers size={25} className="nav-item"/></Nav.Link>
		</Fragment>

		:

		<Fragment>
			<Nav.Link href="#features" className="mx-4"><FaShoppingCart size={25} className="nav-item"/></Nav.Link>
		</Fragment>

		: null

		return (

			<Navbar collapseOnSelect expand="lg" className= "navBar" fixed="top">
			  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
			  <Navbar.Collapse id="responsive-navbar-nav" className="mx-5">
			    <Form inline className="ml-5">
					<FormControl type="text" placeholder="Search" className="px-2 search" style={{
						height: "40px",
						width: "80%",
						outline: "none",
						background: "none",
						border: "2px solid #55527C",
						marginRight: "-35px"
					}}/>
	 				<button className="btn0" type="submit"><BsSearch/></button>
			 	</Form>
			    <Nav className="ml-auto px-4">
			    	<Nav.Link href="/" className="mx-4"><FaHome size={25} className="nav-item"/></Nav.Link>
			    	{cart}
			       	<NavDropdown title=<VscAccount size={25} className="nav-item"/> id="collasible-nav-dropdown" className="mx-4">
				        {dropNav}
			      	</NavDropdown>
			    </Nav>
			  </Navbar.Collapse>
			{/*LOG In MODAL*/}
			<Modal show={showLogin} onHide={closeLogin} style={{transition: 'all 150ms ease-in-out'}}>
		        <Modal.Header closeButton></Modal.Header>
		        <Modal.Body>
		        	<div className="container-login">
		        		<h1 className="text-center loginHeader">Log in</h1>
		        		<Form className="form" onSubmit={(e)=>login(e)} >
		        		  <Form.Group className="mb-3 input-field" controlId="formBasicEmail">
		        		    <Form.Control type="email" value={email}
		        		    onChange={(e)=> setEmail(e.target.value)} className="input" required/>
		        		    <Form.Label>Email</Form.Label>
		        		  </Form.Group>

		        		  <Form.Group className="mb-3 input-field" controlId="formBasicPassword">
		        		    <Form.Control type="password" value={password}
		        		    onChange={(e)=>setPassword(e.target.value)} className="input" required/>
		        		    <span className="show">SHOW</span>
		        		    <Form.Label>Password</Form.Label>
		        		  </Form.Group>

		        		  <div className="button mb-3">
		        	         <div className="inner"></div>
		        	         <button type="submit">LOGIN</button>
		        	      </div>

		        		  <p>Not yet a member? <a href="#" onClick={openSignup}>Sign up now</a></p>
		        		</Form>
		        	</div>
		        </Modal.Body>
			</Modal>
			{/*SIGN UP MODAL*/}
			<Modal show={showSignup} onHide={closeSignup} style={{transition: 'all 150ms ease-in-out'}}>
		        <Modal.Header closeButton></Modal.Header>
    		        <Modal.Body>
    		        	<div className="container-login">
    		        		<h1 className="text-center loginHeader">Sign up</h1>
    		        		<Form className="form" onSubmit={(e) => register(e)}>

		        				<Form.Group className="mb-3 input-field" controlId="formBasicEmail">
		        			    	<Form.Control type="text" value={firstName}
		        			    	onChange={(e)=> setFirstName(e.target.value)} className="input" required/>
		        			    	<Form.Label>First Name</Form.Label>
		        				</Form.Group>

	        					<Form.Group className="mb-3 input-field" controlId="formBasicEmail">
	        				    	<Form.Control type="text" value={lastName}
	        				    	onChange={(e)=> setLastName(e.target.value)} className="input" required/>
	        				    	<Form.Label>Last Name</Form.Label>
	        					</Form.Group>

	        					<Form.Group className="mb-3 input-field" controlId="formBasicEmail">
	        				    	<Form.Control type="email" value={email}
	        				    	onChange={(e)=> setEmail(e.target.value)} className="input" required/>
	        				    	<Form.Label>Email</Form.Label>
	        					</Form.Group>

    		        			<Form.Group className="mb-3 input-field" controlId="formBasicPassword">
    		        		    	<Form.Control type="password" value={password}
    		        		    	onChange={(e)=>setPassword(e.target.value)} className="input" required/>
    		        		    		<span className="show">SHOW</span>
    		        		    	<Form.Label>Password</Form.Label>
    		        			</Form.Group>

		        				<Form.Group className="mb-3 input-field" controlId="formBasicPassword">
		        			    	<Form.Control type="password" value={verifyPassword}
		        			    	onChange={(e)=>setverifyPassword(e.target.value)} className="input" required/>
		        			    		<span className="show">SHOW</span>
		        			    	<Form.Label>Confirm Password</Form.Label>
		        				</Form.Group>

    		        			<div className="button mb-3">
    		        	        	<div className="inner"></div>
    		        	        	<button type="submit">SIGN UP</button>
    		        	    	</div>
    		        		</Form>
    		        	</div>
    		        </Modal.Body>
			</Modal>
			</Navbar>


		)
	}