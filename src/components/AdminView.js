import React, {useState, useEffect, useRef, Fragment} from 'react';
// ICONS
import { IoArchive } from "react-icons/io5";
import { MdDelete,MdUnarchive } from "react-icons/md";
import { BiEdit } from "react-icons/bi";
//image
import img from './../images/alison.jpg'
// bootstrap
import {Modal, Form, Button} from 'react-bootstrap'
//Swal
import Swal from 'sweetalert2';

export default function AdminView(props){

	const {productData, fetchData} = props;

	const [productId, setProductId] = useState('');
	const [products, setProducts] = useState([]);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	let token = localStorage.getItem('token') 

	useEffect(() => {

		const productsArr = productData.map((product)=> {
			return (
				<div class="box" key={product._id}>
					<div class="image" >
						<img src={img} />
					</div>
					<div class="info">
						<h3 class="title">{product.name}</h3>
						<div class="subInfo">
							<div class="price">₱{product.price}</div>
							<div class="stars">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star-half"></i>
							</div>
						</div>
					</div>
					<div class="overlay">
						<a href="#" style={{'--i':1}} onClick={ ()=> openEdit(product._id) }><BiEdit/></a>
						{
							(product.isActive === true) ?
							<a href="#" style={{'--i':2}} onClick={()=> archiveToggle(product._id, product.isActive)}><IoArchive/></a> :
							<a href="#" style={{'--i':2}} onClick={() => unarchiveToggle(product._id, product.isActive)}><MdUnarchive/></a>
						}
						<a href="#" style={{'--i':3}} onClick={ () => deleteToggle(product._id)}><MdDelete/></a>
					</div>
				</div>
			)
		})
		setProducts(productsArr)
	}, [productData])

	//Edit a product
	const openEdit = (productId) => {
		fetch(`https://salty-scrubland-98294.herokuapp.com/api/products/${productId}`,{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setProductId(result._id);
			setName(result.name);
			setDescription(result.description);
			setPrice(result.price)
		})
		setShowEdit(true)
	}
	const closeEdit = () => {
		setShowEdit(false);
		setName("");
		setDescription("");
		setPrice("")
	}
	const editProduct = (e, productId) => {
		e.preventDefault()
		fetch(`https://salty-scrubland-98294.herokuapp.com/api/products/${productId}/update`,{
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(result => result.json())
		.then(result => {
			fetchData()
			if(typeof result !== "undefined"){

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated!"
				})

				closeEdit();
			} else {
				fetchData()
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		})
	}
	// Archive
	const archiveToggle = (productId, isActive) => {
		fetch(`https://salty-scrubland-98294.herokuapp.com/api/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(result => {
			fetchData()
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					"text": "Product successfully archived"
				})
			} else {
				fetchData();
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					"text": "Please try again"
				})
			}
		})
	}
	// unarchive
	const unarchiveToggle = (productId, isActive) => {
		fetch(`https://salty-scrubland-98294.herokuapp.com/api/products/${productId}/unarchive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			fetchData();
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					"text": "Product successfully unarchived"
				})
			} else {
				fetchData();
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					"text": "Please try again"
				})
			}
		})
	}
	// delete
	const deleteToggle = (productId) => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.isConfirmed) {
		    fetch(`https://salty-scrubland-98294.herokuapp.com/api/products/${productId}/delete`, {
    			method: "DELETE",
    			headers: {
    				"Authorization": `Bearer ${token}`
    			}
    		})
    		.then(result => result.json())
    		.then(result => {
    			console.log(result)

    			fetchData();
    			if(result === true){
    				Swal.fire({
    					title: "Success",
    					icon: "success",
    					"text": "Product successfully deleted"
    				})
    			} else {
    				fetchData();
    				Swal.fire({
    					title: "Something went wrong",
    					icon: "error",
    					"text": "Please try again"
    				})
    			}
    		})
		  }
		})
	}
	// ADD a product
	const addProduct =(e) => {
		e.preventDefault()
		fetch('https://salty-scrubland-98294.herokuapp.com/api/products/addProduct', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			if(result === true){
				fetchData()

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added"
				})

				setName("")
				setDescription("")
				setPrice(0)

				closeAdd();

			} else {
				fetchData();

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong"
				})
			}
		})
	}

	return(
		<div id="productPage">
			<h1 className="text-center my-3">Products</h1>
			<div className="d-flex justify-content-center mb-2">
				<Button onClick={openAdd} style={{
					display: 'inline-block',
					padding: '10px 30px',
					background: '#3a4052',
					color: '#fff',
					borderRadius: '5px',
					border: 'solid #fff 1px',
					marginTop: '25px',
					opacity: '0.7'
				}}>Add New Product</Button>
			</div>
		<div style={{display: 'flex', alignItems: 'center', justifyContent: 'space-evenly',
					flexWrap: 'wrap', background: 'none'}}>
			{products}
		{/*EDIT MODAL*/}
		<Modal show={showEdit} onHide={closeEdit}>
			<Form onSubmit={ (e) => editProduct(e, productId)}>
				<Modal.Header>
					<Modal.Title>Edit Product</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group controlId="productName">
						<Form.Label>Name</Form.Label>
						<Form.Control
							type="text"
							value={name}
							onChange={ (e)=> setName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group controlId="productDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control
							type="text"
							value={description}
							onChange={ (e)=> setDescription(e.target.value)}
						/>
					</Form.Group>
					<Form.Group controlId="productPrice">
						<Form.Label>Price</Form.Label>
						<Form.Control
							type="number"
							value={price}
							onChange={ (e)=> setPrice(e.target.value)}
						/>
					</Form.Group>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={closeEdit}>Close</Button>
					<Button variant="success" type="submit">Submit</Button>
				</Modal.Footer>
			</Form>
		</Modal>
		{/*Add product*/}
		<Modal show={showAdd} onHide={closeAdd}>
			<Form onSubmit={ (e) => addProduct(e) }>
				<Modal.Header>Add Product</Modal.Header>
				<Modal.Body>
					<Form.Group productId="productName">
						<Form.Label>Name</Form.Label>
						<Form.Control 
							type="text"
							value={name}
							onChange={(e)=> setName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group productId="productDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control
							type="text"
							value={description}
							onChange={(e)=> setDescription(e.target.value)}
						/>
					</Form.Group>
					<Form.Group productId="productPrice">
						<Form.Label>Price</Form.Label>
						<Form.Control 
							type="number"
							value={price}
							onChange={(e)=> setPrice(e.target.value)}
						/>
					</Form.Group>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={closeAdd}>Close</Button>
					<Button variant="success" type="submit">Submit</Button>
				</Modal.Footer>
			</Form>
		</Modal>
		</div>
		</div>
	)
}