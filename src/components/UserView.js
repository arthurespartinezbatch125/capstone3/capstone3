import React, {useState, useEffect} from 'react';
import 'react-bootstrap'
import Product from './Product'

export default function UserView({productData}){
	const[products, setProducts] = useState([])

	useEffect(() => {
		const productsArr = productData.map((product)=> {

			if(product.isActive === true){
				return <Product key={product._id} productProp = {product}/>
			} else {
				return null
			}
		})
		setProducts(productsArr)
	}, [productData])

	return(
		<div id="productPage">
			<h1 className="text-center my-3">Products</h1>
			<div id="productSection">
				{products}
			</div>
		</div>
	)
}