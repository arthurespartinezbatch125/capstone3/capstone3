import React from 'react';

import video from './../videos/pexels2.mp4'

export default function Banner(){


	return(
		<section className="showcase">
			<div className="video-container">
				<video src={video} autoPlay muted loop></video>
			</div>
			<div className="video-content">
				<div id="home-heading">
					<h1 id="home-heading-1">arthur</h1>
					<h1 id="home-heading-2">toy<span> kingdom</span></h1>
				</div>
				<a href="#productPage" className="video-btn" onClick={e => {
					let productPage = document.getElementById("productPage");
					e.preventDefault();
					productPage && productPage.scrollIntoView()}} >Shop Now</a>
			</div>
		</section>
	)
}