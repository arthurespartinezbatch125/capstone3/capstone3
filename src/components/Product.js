import React, {useContext, useState,} from 'react';
import {useHistory} from 'react-router-dom';
// ICONS,
import {FaShoppingCart} from 'react-icons/fa';
import { CgDetailsMore } from "react-icons/cg";
//image
import img from './../images/alison.jpg'
//usercontext
import UserContext from './../UserContext';
// alert
import Swal from 'sweetalert2';
import { Form, Modal} from 'react-bootstrap';

export default function Product({productProp}){

	const {name, price, description, _id} = productProp

	const [email,setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [firstName,setFirstName] = useState('');
	const [lastName,setLastName] = useState('');
	const [verifyPassword, setverifyPassword] = useState('');

	const {user, setUser} = useContext(UserContext);
	const [showLogin, setShowLogin] = useState(false);
	const [showSignup, setShowSignup] = useState(false);

	const openLogin = () => setShowLogin(true);
	const closeLogin = () => setShowLogin(false);
	const openSignup = () => {
		setShowSignup(true);
		setShowLogin(false)
	}
	const closeSignup = () => setShowSignup(false);
	let history = useHistory();

	// LOGIN
	function login(e){
		e.preventDefault();

		fetch('https://salty-scrubland-98294.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(result => {

			if(typeof result.access !== "undefined"){
				//What should we do with the aceess token
				localStorage.setItem('token', result.access)
				userDetails(result.access);
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "You have succesfully log in"
				})

			} else {
				fetch('https://salty-scrubland-98294.herokuapp.com/api/users/checkEmail', {
					method: "POST",
					headers:{
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						email: email
					})
				})
				.then(result => result.json())
				.then(result => {
					if(result === false){
						Swal.fire({
							title: "Email does not exist",
							icon: "error",
							text: "You do not have an existing account"
						})
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})
					}
				})
			}

		})

		const userDetails = (token) => {
			fetch('https://salty-scrubland-98294.herokuapp.com/api/users/details', {
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result=>result.json())
			.then(result => {
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			})
		}

		setEmail('');
		setPassword('');
		setShowLogin(false)
	}

	// Sign up
	function register(e){
		e.preventDefault();

		fetch('https://salty-scrubland-98294.herokuapp.com/api/users/checkEmail',{
				method: "POST",
				headers:{
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(result => result.json())
			.then(result =>{
				if(result ===  true){
					Swal.fire({
						title: 'Duplicate email found' ,
						icon: 'error' ,
						text: 'Please choose another email'
					})
				} else if (password !== verifyPassword) {
					Swal.fire({
						title: 'Incorrect password' ,
						icon: 'error' ,
						text: 'Password don\'t match'
					})
				} else {
					fetch('https://salty-scrubland-98294.herokuapp.com/api/users/register',{
						method: "POST",
						headers: {
							"Content-Type": 'application/json',
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password
						})
					})
					.then(result => result.json())
					.then(result => {
						if(result === true){
							Swal.fire({
								title: 'Registration Succesful',
								icon: 'success',
								text: 'You may now proceed to log in'
							})

							history.push('/');
						} else {
							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})
						}
					})
				}
			})

		setFirstName('');
		setLastName('');
		setEmail('');
		setPassword('');
		setverifyPassword('');
		setShowSignup(false)
	}

	return (
			<div class="box">
				<div class="image">
					<img src={img} alt=""/>
				</div>
				<div class="info">
					<h3 class="title">{name}</h3>
					<div class="subInfo">
						<div class="price">₱{price}</div>
						<div class="stars">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star-half"></i>
						</div>
					</div>
				</div>
				<div class="overlay">
					{(user.id === null) ? <a href="#" style={{'--i':1}} onClick={openLogin}><FaShoppingCart/></a> :
					<a href="#" style={{'--i':1}} ><FaShoppingCart/></a>}
					<a href="#" style={{'--i':2}} ><CgDetailsMore/></a>
				</div>
				{/*LOG In MODAL*/}
				<Modal show={showLogin} onHide={closeLogin} style={{transition: 'all 150ms ease-in-out'}}>
			        <Modal.Header closeButton></Modal.Header>
			        <Modal.Body>
			        	<div className="container-login">
			        		<h1 className="text-center loginHeader">Log in</h1>
			        		<Form className="form" onSubmit={(e)=>login(e)} >
			        		  <Form.Group className="mb-3 input-field" controlId="formBasicEmail">
			        		    <Form.Control type="email" value={email}
			        		    onChange={(e)=> setEmail(e.target.value)} className="input" required/>
			        		    <Form.Label>Email</Form.Label>
			        		  </Form.Group>

			        		  <Form.Group className="mb-3 input-field" controlId="formBasicPassword">
			        		    <Form.Control type="password" value={password}
			        		    onChange={(e)=>setPassword(e.target.value)} className="input" required/>
			        		    <span className="show">SHOW</span>
			        		    <Form.Label>Password</Form.Label>
			        		  </Form.Group>

			        		  <div className="button mb-3">
			        	         <div className="inner"></div>
			        	         <button type="submit">LOGIN</button>
			        	      </div>

			        		  <p>Not yet a member? <a href="#" onClick={openSignup}>Sign up now</a></p>
			        		</Form>
			        	</div>
			        </Modal.Body>
				</Modal>
				{/*SIGN UP MODAL*/}
				<Modal show={showSignup} onHide={closeSignup} style={{transition: 'all 150ms ease-in-out'}}>
			        <Modal.Header closeButton></Modal.Header>
	    		        <Modal.Body>
	    		        	<div className="container-login">
	    		        		<h1 className="text-center loginHeader">Sign up</h1>
	    		        		<Form className="form" onSubmit={(e) => register(e)}>

			        				<Form.Group className="mb-3 input-field" controlId="formBasicEmail">
			        			    	<Form.Control type="text" value={firstName}
			        			    	onChange={(e)=> setFirstName(e.target.value)} className="input" required/>
			        			    	<Form.Label>First Name</Form.Label>
			        				</Form.Group>

		        					<Form.Group className="mb-3 input-field" controlId="formBasicEmail">
		        				    	<Form.Control type="text" value={lastName}
		        				    	onChange={(e)=> setLastName(e.target.value)} className="input" required/>
		        				    	<Form.Label>Last Name</Form.Label>
		        					</Form.Group>

		        					<Form.Group className="mb-3 input-field" controlId="formBasicEmail">
		        				    	<Form.Control type="email" value={email}
		        				    	onChange={(e)=> setEmail(e.target.value)} className="input" required/>
		        				    	<Form.Label>Email</Form.Label>
		        					</Form.Group>

	    		        			<Form.Group className="mb-3 input-field" controlId="formBasicPassword">
	    		        		    	<Form.Control type="password" value={password}
	    		        		    	onChange={(e)=>setPassword(e.target.value)} className="input" required/>
	    		        		    		<span className="show">SHOW</span>
	    		        		    	<Form.Label>Password</Form.Label>
	    		        			</Form.Group>

			        				<Form.Group className="mb-3 input-field" controlId="formBasicPassword">
			        			    	<Form.Control type="password" value={verifyPassword}
			        			    	onChange={(e)=>setverifyPassword(e.target.value)} className="input" required/>
			        			    		<span className="show">SHOW</span>
			        			    	<Form.Label>Confirm Password</Form.Label>
			        				</Form.Group>

	    		        			<div className="button mb-3">
	    		        	        	<div className="inner"></div>
	    		        	        	<button type="submit">SIGN UP</button>
	    		        	    	</div>
	    		        		</Form>
	    		        	</div>
	    		        </Modal.Body>
				</Modal>
			</div>

	)
}